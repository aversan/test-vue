import { defineConfig } from 'vite-plugin-windicss'
import typography from 'windicss/plugin/typography'
import lineClamp from 'windicss/plugin/line-clamp'
import aspectRatio from 'windicss/plugin/aspect-ratio'

const defaultTheme = require('windicss/defaultTheme')

export default defineConfig({
  darkMode: 'class',
  safelist: ['prose'],
  theme: {
    extend: {
      screens: {
        md: '768px',
        xl: '1280px',
      },
      colors: {
        white: '#fff',
        black: '#000',
        gray: {
          // 'gray-100': '#f8f9fa',
          '100': '#E8E8E8',
          '150': '#E5E5E5',
          '200': '#E2E2E2',
          '300': '#D1D1D6',
          // 'gray-400': '#ced4da',
          // 'gray-500': '#adb5bd',
          // 'gray-600': '#6c757d',
          // 'gray-700': '#495057',
          '800': '#3C3C43',
          // 'gray-900': '#212529',
        },
        primary: {
          default: '#FF008A',
        },
      },
      gridTemplateColumns: {
        header: '50% 1fr 166px',
        xxx: '33.333% 33.333% 33.333%',
      },
      fontFamily: {
        sans: ['Manrope', ...defaultTheme.fontFamily.sans],
        alt: ['Inter', ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        // xs: ['0.75rem', { lineHeight: '1rem' }],
        sm: ['0.875rem', { lineHeight: '.875rem' }],
        base: ['1rem', { lineHeight: '1.5rem' }],
        lg: ['1.125rem', { lineHeight: '1.875rem' }],
        xl: ['1.25rem', { lineHeight: '1.5rem' }],
        '2xl': ['1.5rem', { lineHeight: '2rem' }],
        // '3xl': ['1.875rem', { lineHeight: '2.25rem' }],
        '3.5xl': ['2rem', { lineHeight: '2.25rem' }],
        '4xl': ['2.125rem', { lineHeight: '2.875rem' }],
        // '5xl': ['3rem', { lineHeight: '1' }],
        // '6xl': ['3.75rem', { lineHeight: '1' }],
        // '7xl': ['4.5rem', { lineHeight: '1' }],
        // '8xl': ['6rem', { lineHeight: '1' }],
        // '9xl': ['8rem', { lineHeight: '1' }],
      },
      lineHeight: {
        // none: '1',
        tight: '1.222',
        snug: '1.3333',
        // normal: '1.5',
        // relaxed: '1.625',
        // loose: '2',
        // 3: '.75rem',
        // 4: '1rem',
        5: '1.25rem',
        '5.5': '1.375rem',
        6: '1.5rem',
        7: '1.75rem',
        7.5: '1.875rem',
        8: '2rem',
        9: '2.25rem',
        // 10: '2.5rem',
      },
      borderRadius: {
        // none: '0px',
        // sm: '0.125rem',
        DEFAULT: '0.25rem',
        md: '0.3125rem', // 5px
        lg: '0.625rem', // 10px
        // xl: '0.75rem',
        // '2xl': '1rem',
        '2.5xl': '1.25rem', // 20px
        // '3xl': '1.5rem',
        // full: '9999px',
      },
      spacing: {
        // px: '1px',
        // 0: '0px',
        // 0.5: '0.125rem',
        // 1: '0.25rem',
        // 1.5: '0.375rem',
        // 2: '0.5rem',
        2.5: '0.625rem',
        // 3: '0.75rem',
        3.125: '0.78125rem',
        // 3.5: '0.875rem',
        // 4: '1rem',
        // 5: '1.25rem',
        5.5: '1.375rem', // 22px
        // 6: '1.5rem',
        6.25: '1.5625rem', // 25px
        // 7: '1.75rem',
        7.5: '1.875rem', // 30px
        // 8: '2rem',
        // 9: '2.25rem',
        // 10: '2.5rem',
        // 11: '2.75rem',
        // 12: '3rem',
        12.5: '3.125rem',
        // 14: '3.5rem',
        // 16: '4rem',
        // 20: '5rem',
        22: '5.5rem',
        22.5: '5.625rem',
        // 24: '6rem',
        24.5: '6.125rem', // 98px
        // 28: '7rem',
        // 32: '8rem',
        // 36: '9rem',
        // 40: '10rem',
        // 44: '11rem',
        // 48: '12rem',
        // 52: '13rem',
        // 56: '14rem',
        // 60: '15rem',
        // 64: '16rem',
        70: '17.5rem',
        71.25: '17.8125rem',
        // 72: '18rem',
        // 80: '20rem',
        // 96: '24rem',
      },
    },
  },
  plugins: [
    typography,
    lineClamp,
    aspectRatio,
  ],
})
