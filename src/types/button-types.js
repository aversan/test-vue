export const sizes = {
  MEDIUM: 'medium',
  SMALL: 'small',
}

export const types = {
  PRIMARY: 'primary',
  OUTLINE_PRIMARY: 'outline-primary',
  TEXT: 'text',
  TEXT_PRIMARY: 'text-primary',
}

export const icons = {
  LEFT_ICON: 'left-icon',
  RIGHT_ICON: 'right-icon',
  ONLY_ICON: 'only-icon',
}

export const radiuses = {
  SEMI_ROUNDED: 'semi-rounded',
  PILL: 'pill',
}
