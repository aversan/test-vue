import { computed } from 'vue'
import { useMq as Mq } from 'vue3-mq'

export default function useMq() {
  const mq = Mq()

  const isMobile = computed(() => {
    return mq.current === 'phone'
  })

  const isTablet = computed(() => {
    return mq.current === 'tablet'
  })

  const isMobileOrTablet = computed(() => {
    return mq.current === 'phone' || mq.current === 'tablet'
  })

  const isTabletOrDesktop = computed(() => {
    return mq.current === 'tablet' || mq.current === 'desktop'
  })

  const isDesktop = computed(() => {
    return mq.current === 'desktop'
  })

  const screen = computed(() => {
    return mq.current
  })

  return {
    mq,
    isMobile,
    isMobileOrTablet,
    isTablet,
    isTabletOrDesktop,
    isDesktop,
    screen,
  }
}
