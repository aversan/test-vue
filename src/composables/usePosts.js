import { onMounted, ref } from 'vue'
import axios from 'axios'
import { nanoid } from 'nanoid'

export default function usePosts({ limit, page }) {
  const posts = ref([])
  const totalPages = ref(0)
  const isPostsLoading = ref(true)

  const fetching = async () => {
    try {
      const postsResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts`, {
        params: {
          _page: page.value,
          _limit: limit,
        },
      })

      totalPages.value = Math.ceil(postsResponse.headers['x-total-count'] / limit)

      posts.value = postsResponse.data.map(post => {
        const { id, title, body } = post

        return {
          uuid: nanoid(),
          id,
          image: 'https://via.placeholder.com/310x250',
          title,
          text: body,
        }
      })
    } catch (e) {
      console.log(e)
    } finally {
      isPostsLoading.value = false
    }
  }

  const loadMorePosts = async () => {
    try {
      const postsResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts`, {
        params: {
          _page: page.value,
          _limit: limit,
        },
      })

      totalPages.value = Math.ceil(postsResponse.headers['x-total-count'] / limit)

      posts.value = [
        ...posts.value,
        ...postsResponse.data.map(post => {
          const { id, title, body } = post

          return {
            uuid: nanoid(),
            id,
            image: 'https://via.placeholder.com/310x250',
            title,
            text: body,
          }
        }),
      ]
    } catch (e) {
      console.log(e)
    } finally {
      isPostsLoading.value = false
    }
  }

  onMounted(fetching)

  return {
    posts,
    isPostsLoading,
    totalPages,
    fetching,
    loadMorePosts,
  }
}
