import { onMounted, ref } from 'vue'
import axios from 'axios'
import { nanoid } from 'nanoid'

export default function usePostComments({ postId }) {
  const comments = ref([])
  const isCommentsLoading = ref(true)

  const fetching = async () => {
    try {
      const commentsResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`)

      comments.value = commentsResponse.data.map(comment => {
        const { id, name, body } = comment

        return {
          uuid: nanoid(),
          id,
          avatar: 'https://via.placeholder.com/36x36',
          name,
          text: body,
        }
      })
    } catch (e) {
      console.log(e)
    } finally {
      isCommentsLoading.value = false
    }
  }

  onMounted(fetching)

  return {
    comments,
    isCommentsLoading,
  }
}
