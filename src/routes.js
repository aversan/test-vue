import HomePage from './views/home.vue'
import PostPage from './views/post.vue'
import NotFoundPage from './views/not-found.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
  { path: '/', component: HomePage, meta: { title: 'Посты' } },
  { path: '/posts/:id', component: PostPage, meta: { title: 'Пост' } },
  { path: '/:path(.*)', component: NotFoundPage },
]
