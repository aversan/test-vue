import { ref } from 'vue'

export default function usePagination(page) {
  const changePage = pageNumber => {
    page.value = pageNumber
  }

  return {
    changePage,
  }
}
