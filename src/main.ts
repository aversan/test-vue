import { createApp } from 'vue'
import VWave from 'v-wave'
import { Vue3Mq } from 'vue3-mq'
import App from './app.vue'
import { routes } from './routes'
import { createRouter, createWebHistory } from 'vue-router'
import store from './store/index.js';

import 'virtual:windi.css'

const router = createRouter({
  history: createWebHistory(),
  routes,
})

createApp(App)
  .use(VWave, {
    color: 'white',
    initialOpacity: 0.2,
    easing: 'ease-in',
  })
  .use(Vue3Mq, {
    breakpoints: {
      phone: 0,
      tablet: 768,
      desktop: 1280,
    },
  })
  .use(router)
  .use(store)
  .mount('#app')
