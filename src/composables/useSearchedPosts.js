import { computed, ref } from 'vue'

export const searchQuery = ref('')

export default function useSearchedPosts(posts) {
  const searchedPosts = computed(() => {
    return posts.value.filter(post => post.title.toLowerCase().includes(searchQuery.value.toLowerCase()))
  })

  return {
    searchedPosts,
  }
}
