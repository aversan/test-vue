import IconPencil from './icon-pencil.vue'
import IconSearch from './icon-search.vue'
import IconPost from './icon-post.vue'
import IconPlus from './icon-plus.vue'
import IconComment from './icon-comment.vue'
import IconChevronLeft from './icon-chevron-left.vue'
import IconChevronRight from './icon-chevron-right.vue'

export { IconPencil, IconSearch, IconPost, IconPlus, IconComment, IconChevronLeft, IconChevronRight }
