import { computed, ref } from 'vue'
import classNames from 'classnames'
import { states, sizes } from '@/types/input-types'
import { nanoid } from 'nanoid'

export default function useInput(props) {
  const id = nanoid()
  const value = ref('')
  const hover = ref(false)
  const focus = ref(false)

  // extra states
  const DISABLED = 'disabled'
  const HOVER = 'hover'
  const FOCUS = 'focus'
  const NONE = 'none'

  const filled = computed(() => {
    return value.value !== ''
  })

  const extraState = computed(() => {
    const { disabled } = props

    if (disabled) {
      return DISABLED
    }

    if (focus.value) {
      return FOCUS
    }

    if (hover.value) {
      return HOVER
    }

    return NONE
  })

  const containerClasses = computed(() => {
    const { disabled } = props

    return classNames('input-container relative w-full cursor-text flex items-center space-x-2 focus:outline-none', {
      'pointer-events-none cursor-not-allowed opacity-50': disabled,
    })
  })

  const inputClasses = computed(() => {
    const { size } = props
    const { SMALL, MEDIUM } = sizes

    return classNames(
      'input appearance-none w-full text-base focus:outline-none bg-transparent self-stretch truncate border-0 focus:outline-none',
      {
        'py-2': [SMALL, MEDIUM].includes(size),
      }
    )
  })

  const stateClasses = computed(() => {
    const { state } = props
    const { DEFAULT } = states

    return classNames({
      'input-state-default bg-gray-800 bg-opacity-10 text-black': state === DEFAULT && extraState.value === NONE,
      [`input-state-default-${extraState.value} border border-black bg-white text-black`]:
        state === DEFAULT && [HOVER, FOCUS].includes(extraState.value),
    })
  })

  const sizeClasses = computed(() => {
    const { size } = props
    const { SMALL, MEDIUM } = sizes

    return classNames({
      'input-size-medium px-2 rounded-lg h-12.5': size === MEDIUM,
      'input-size-small px-2 rounded-lg h-9': size === SMALL,
    })
  })

  const wrapperClasses = computed(() => {
    const { size } = props
    const { SMALL, MEDIUM } = sizes

    return classNames('relative flex flex-1', {
      'h-12.5': size === MEDIUM,
      'h-9': size === SMALL,
    })
  })

  const iconClasses = computed(() => {
    return classNames('w-5.5 h-5.5 flex-shrink-0 input-icon')
  })

  const iconStateClasses = computed(() => {
    const { state } = props
    const { DEFAULT } = states

    return classNames({
      'input-icon-state-default text-gray-800 text-opacity-60': state === DEFAULT,
    })
  })

  const actionClasses = computed(() => {
    return classNames('w-5.5 h-5.5 flex-shrink-0 input-action cursor-pointer')
  })

  const actionStateClasses = computed(() => {
    const { state } = props
    const { DEFAULT } = states

    return classNames({
      'input-action-state-default text-gray-800 text-opacity-60 hover:text-black': state === DEFAULT,
    })
  })

  const labelClasses = computed(() => {
    const { size } = props
    const { SMALL, MEDIUM } = sizes

    return classNames(
      'max-w-full absolute top-0 py-2 inline-flex items-center whitespace-no-wrap overflow-hidden transition-all duration-300 cursor-text text-base',
      {
        'h-12.5': size === MEDIUM,
        'h-9': size === SMALL,
      }
    )
  })

  const labelStateClasses = computed(() => {
    const { state } = props
    const { DEFAULT } = states

    return classNames({
      'label-state-default text-gray-800 text-opacity-60': state === DEFAULT,
    })
  })

  return {
    id,
    value,
    hover,
    focus,
    filled,
    containerClasses,
    stateClasses,
    sizeClasses,
    wrapperClasses,
    inputClasses,
    iconClasses,
    iconStateClasses,
    actionClasses,
    actionStateClasses,
    labelClasses,
    labelStateClasses,
  }
}
