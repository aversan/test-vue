export const states = {
  DEFAULT: 'default',
}

export const sizes = {
  SMALL: 'small',
  MEDIUM: 'medium',
}
