import { computed } from 'vue'
import classNames from 'classnames'
import { sizes, types, radiuses } from '@/types/button-types'

export default function useButtonClasses(props) {
  const baseClasses = 'button align-top truncate transition-all duration-150 relative focus:outline-none'

  const radiusClasses = computed(() => {
    const { radius } = props
    const { SEMI_ROUNDED, PILL } = radiuses

    return classNames({
      'button-pill rounded-full': radius === PILL,
      'button-semi-rounded rounded-md': radius === SEMI_ROUNDED,
    })
  })

  const sizeClasses = computed(() => {
    const { size } = props
    const { SMALL, MEDIUM } = sizes

    const sizeClassMap = {
      [MEDIUM]: 'button-medium h-12.5 px-6.25 space-x-3.125',
      [SMALL]: 'button-small h-8 px-4 space-x-2',
    }

    const classes = sizeClassMap[size]

    return classNames({
      [classes]: classes,
    })
  })

  const typeClasses = computed(() => {
    const { type } = props
    const { PRIMARY, OUTLINE_PRIMARY, TEXT, TEXT_PRIMARY } = types

    const typeClassMap = {
      [PRIMARY]: 'button-primary bg-primary-default hover:bg-primary-default focus:bg-primary-default text-white',
      [OUTLINE_PRIMARY]:
        'button-outline-primary bg-white hover:bg-white focus:bg-white text-primary-default border border-primary-default',
      [TEXT]: 'button-text bg-transparent text-black',
      [TEXT_PRIMARY]: 'button-text-primary bg-transparent text-primary-default',
    }

    const classes = typeClassMap[type]

    return classNames({
      [classes]: classes,
    })
  })

  return {
    baseClasses,
    radiusClasses,
    sizeClasses,
    typeClasses,
  }
}
