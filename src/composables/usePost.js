import { onMounted, ref } from 'vue'
import axios from 'axios'
import { nanoid } from 'nanoid'

export default function usePost({ postId }) {
  const post = ref({})
  const isPostLoading = ref(true)

  const fetching = async () => {
    try {
      const postResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}`)

      const { id, title, body } = postResponse.data

      post.value = {
        uuid: nanoid(),
        id,
        image: 'https://via.placeholder.com/310x250',
        title,
        text: body,
      }
    } catch (e) {
      console.log(e)
    } finally {
      isPostLoading.value = false
    }
  }

  onMounted(fetching)

  return {
    post,
    isPostLoading,
  }
}
