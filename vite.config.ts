import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import PluginWindiCSS from 'vite-plugin-windicss'
import ViteFonts from 'vite-plugin-fonts'
import path from 'path'

const pathResolve = pathStr => {
  return path.resolve(__dirname, pathStr)
}

export default defineConfig({
  plugins: [
    PluginWindiCSS(),
    vue(),
    ViteFonts({
      google: {
        families: [
          {
            name: 'Manrope',
            styles: 'wght@300;400;500;600;700',
          },
          {
            name: 'Inter',
            styles: 'wght@400;700',
          },
        ],
      },
    }),
  ],
  build: {
    lib: {
      entry: pathResolve('src/plugin.ts'),
      name: 'gunza-ui',
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
  resolve: {
    alias: {
      '@': pathResolve('./src'),
      '~': pathResolve('./node_modules'),
    },
  },
  server: {
    open: true,
  },
})
