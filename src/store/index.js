import { createStore } from 'vuex'
import axios from 'axios'
import { nanoid } from 'nanoid'
import snakeCaseKeys from 'snakecase-keys'
import camelCaseKeys from 'camelcase-keys'
import { getFormData } from '@/utils/utils'

export default createStore({
  state: {
    posts: [
      {
        uuid: nanoid(),
        id: 1,
        image: 'https://via.placeholder.com/310x250',
        title: 'Lorem ipsum dolor sit amet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
      {
        uuid: nanoid(),
        id: 2,
        image: 'https://via.placeholder.com/310x250',
        title: 'Lorem ipsum dolor sit amet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
      {
        uuid: nanoid(),
        id: 3,
        image: 'https://via.placeholder.com/310x250',
        title: 'Lorem ipsum dolor sit amet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
    ],
    post: {
      id: 1,
      image: 'https://via.placeholder.com/973x285',
      title: 'title',
      text: 'text text text1',
    },
  },
  mutations: {
    // updateCartItem(state, updatedItem) {
    //   state.cart = state.cart.map(cartItem => {
    //     if (cartItem.id == updatedItem.id) {
    //       return updatedItem
    //     }
    //
    //     return cartItem
    //   })
    // },
    createPost(state, post) {
      state.posts.push(post)
    },
    removePost(state, post) {
      state.posts = state.posts.filter(item => {
        return item.id !== post.id
      })
    },
    setPosts(state, { posts }) {
      state.posts = posts
    },
  },
  actions: {
    async getPosts({ limit = 10, page = 1 }) {
      if (!process.browser) {
        return
      }

      try {
        // const formData = getFormData({
        //   ...snakeCaseKeys({ _limit: limit, _offset: offset }),
        // })

        const postsResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts`, {
          params: {
            _page: page,
            _limit: limit,
          },
        })

        totalPages = Math.ceil(postsResponse.headers['x-total-count'] / limit)

        const posts = postsResponse.data
        const formattedPosts = posts.map(post => {
          return {
            uuid: nanoid(),
            id: post.id,
            image: 'https://via.placeholder.com/310x250',
            title: post.title,
            text: post.body,
          }
        })

        return [...camelCaseKeys(formattedPosts)]
      } catch (error) {
        console.log(error)
      }
    },
    async getPost({ id = 1 }) {
      if (!process.browser) {
        return
      }

      try {
        // const formData = getFormData({
        //   ...snakeCaseKeys({ _limit: limit, _offset: offset }),
        // })

        const postResponse = await axios.post(`https://jsonplaceholder.typicode.com/posts/${id}`)

        const post = postResponse.data
        const formattedPost = {
          uuid: nanoid(),
          image: 'https://via.placeholder.com/973x285',
          id: post.id,
          title: post.title,
          text: post.body,
        }

        return [...camelCaseKeys(formattedPosts)]
      } catch (error) {
        console.log(error)
      }
    },
  },
})

// import { nanoid } from 'nanoid'
// import camelCaseKeys from 'camelcase-keys'
// import snakeCaseKeys from 'snakecase-keys'
// import { getFormData } from '@/utils/utils'
// import { camelCase, map, chain } from 'lodash'

// export const state = () => ({
//   articles: {
//     about: null,
//   },
//
//   game: {
//     gid: null,
//     gameTitle: null,
//     gameHeight: null,
//     gameSession: null,
//     gameUrl: null,
//   },
//
//   userInfo: {
//     user: null,
//     games: { games: [], offset: 0 },
//     friends: { users: [], offset: 0 },
//     mutualFriends: { users: [], offset: 0 },
//   },
//
//   userFriends: { users: [], offset: 0 },
//   userMutualFriends: { users: [], offset: 0 },
//   userSubscriptions: { users: [], offset: 0 },
//   userFollowers: { users: [], offset: 0 },
//
//   searchUsers: { users: [], offset: 0 },
//   searchUsersQuery: '',
//
//   myFriends: { users: [], offset: 0 },
//   mySubscriptions: { users: [], offset: 0 },
//   myFollowers: { users: [], offset: 0 },
//   myBlacklist: { users: [], offset: 0 },
//
//   myNews: { news: [], offset: 0 },
//
//   games: {
//     all: { games: [], offset: 0 },
//     new: { games: [], offset: 0 },
//     recommended: { games: [], offset: 0 },
//     carousel: { games: [], offset: 0 },
//     recent: { games: [], offset: 0 },
//     myGames: { games: [], offset: 0 },
//     userGames: { games: [], offset: 0 },
//   },
//
//   gamesCategories: { categories: [], selectedCid: null },
//
//   balance: { balance: 0, history: [] },
//   chats: { chats: [], offset: 0 },
//   searchChats: { chats: [], offset: 0 },
//   searchChatsQuery: '',
//   chatInfo: { user: null },
//   groupChatInfo: null,
//   groupChat: null,
//   chatMessages: { messages: [], offset: 0 },
//   notifications: { notifications: [], offset: 0 },
//   personalSettings: {
//     firstName: '',
//     lastName: '',
//     birthDate: '',
//     sex: '',
//     country: '',
//     city: '',
//     birthDay: '',
//     birthMonth: '',
//     birthYear: '',
//   },
//   accessSettings: {
//     email: '',
//   },
//   notificationsSettings: {
//     news: false,
//     friends: false,
//     games: false,
//     friendsMessages: false,
//     usersMessages: false,
//   },
//
//   subjects: [],
//   cities: [],
//   countries: [],
//   supportForm: {
//     subject: '',
//     email: '',
//     name: '',
//     message: '',
//   },
//   apiToken: false,
//   showFooterMobileOrTablet: false,
//   copyright: '© 2020 EXE. WORLD | All rights reserved',
//   footerMenu: [
//     { uuid: nanoid(), text: 'About', href: '/about' },
//     { uuid: nanoid(), text: 'Help', href: '/help' },
//     { uuid: nanoid(), text: 'For Developers', href: '/for-developers' },
//   ],
//   friendMenu: [
//     {
//       uuid: nanoid(),
//       iconName: 'friend-remove',
//       text: 'Remove from Friends',
//       functionName: 'deleteFriend',
//     },
//     {
//       uuid: nanoid(),
//       iconName: 'stop',
//       text: 'Block User',
//       functionName: 'addMyBlacklist',
//     },
//     {
//       uuid: nanoid(),
//       iconName: 'report',
//       text: 'Report',
//       functionName: 'toReport',
//       warning: true,
//     },
//   ],
//   chatMenu: [
//     {
//       uuid: nanoid(),
//       iconName: 'stop',
//       text: 'Block User',
//       functionName: 'addMyBlacklist',
//     },
//     {
//       uuid: nanoid(),
//       iconName: 'history-delete',
//       text: 'Delete History',
//       warning: true,
//       functionName: 'deleteChatMessages',
//     },
//   ],
//   userInfoMenu: [
//     {
//       uuid: nanoid(),
//       iconName: 'stop',
//       text: 'Block User',
//       functionName: 'addMyBlacklist',
//     },
//     {
//       uuid: nanoid(),
//       iconName: 'report',
//       text: 'Report',
//       functionName: 'toReport',
//       warning: true,
//     },
//   ],
//   gameMenu: [
//     { uuid: nanoid(), text: 'Invite Friends to the Game' },
//     { uuid: nanoid(), text: 'Find Friends to Play' },
//     {
//       uuid: nanoid(),
//       text: 'Looking for friends to play',
//       toggle: true,
//       functionName: 'toggleGameFriends',
//     },
//     {
//       uuid: nanoid(),
//       text: 'Send game notifications',
//       toggle: true,
//       functionName: 'toggleGameNotifications',
//     },
//     { uuid: nanoid(), text: 'Terms of use' },
//     { uuid: nanoid(), text: 'About the developer' },
//     {
//       uuid: nanoid(),
//       text: 'Delete from my games',
//       warning: true,
//       functionName: 'removeGame',
//     },
//   ],
//   activeModal: {
//     componentName: null,
//     formVariant: '',
//     show: false,
//   },
//   layout: {
//     hideMenu: false,
//   },
//   userLayout: {
//     hideMenu: false,
//   },
//   routes: null,
//   showModalPage: false,
// })

// export const mutations = {
//   initialStore(state) {
//     if (process.browser && localStorage.getItem('auth._token.local')) {
//       state.apiToken = localStorage.getItem('auth._token.local')
//     }
//   },
//   toReport(state, { uid }) {
//     return { name: 'user-uid-report', params: { uid } }
//   },
//   setActiveModal(state, { componentName, formVariant }) {
//     state.activeModal = {
//       ...state.activeModal,
//       componentName,
//       formVariant,
//       show: true,
//     }
//   },
//   closeActiveModal(state) {
//     state.activeModal.show = false
//   },
//   toggleTheme(state) {
//     if (state.auth.user.theme === '1') {
//       state.auth.user.theme = '0'
//     } else {
//       state.auth.user.theme = '1'
//     }
//   },
//   addUser(state, { user, type }) {
//     const validTypes = [
//       'user-friends',
//       'user-subscriptions',
//       'user-followers',
//       'my-friends',
//       'my-subscriptions',
//       'my-followers',
//       'my-blacklist',
//     ]
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     const { uid } = user
//     const dublicateUser = state[stateName].users.find(
//       (user) => user.uid === uid
//     )
//
//     if (dublicateUser) {
//       return
//     }
//
//     state[stateName].users.push(user)
//   },
//   removeUser(state, { uid, type }) {
//     const validTypes = [
//       'user-friends',
//       'user-subscriptions',
//       'user-followers',
//       'my-friends',
//       'my-subscriptions',
//       'my-followers',
//       'my-blacklist',
//     ]
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     state[stateName].users = state[stateName].users.filter(
//       (user) => user.uid !== uid
//     )
//   },
//   setTheme(state, { value }) {
//     state.auth.user.theme = value
//   },
//   setShowFooterMobileOrTablet(state, show) {
//     state.showFooterMobileOrTablet = show
//   },
//   setApiToken(state, { token }) {
//     state.apiToken = token
//   },
//   resetApiToken(state) {
//     state.apiToken = false
//   },
//   setGame(state, { game }) {
//     state.game = game
//   },
//   addGame(state, { game, type }) {
//     const validTypes = ['user-games', 'my-games']
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     const { gid } = game
//
//     const dublicateGame = state.games[stateName]?.games.find(
//       (game) => game.gid === gid
//     )
//
//     if (dublicateGame) {
//       return
//     }
//
//     state.games[stateName].games.push(game)
//   },
//   removeGame(state, { gid, type }) {
//     const validTypes = ['user-game', 'my-game']
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     state.games[stateName].games = state.games[stateName]?.games.filter(
//       (game) => game.gid !== gid
//     )
//   },
//   setGames(state, { games, type, totalGames, offset = 0 }) {
//     const validTypes = [
//       'all',
//       'new',
//       'recommended',
//       'carousel',
//       'recent',
//       'user-games',
//       'my-games',
//     ]
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     state.games[stateName] = { games, offset, totalGames }
//   },
//   selectedGamesCategories(state, { cid }) {
//     state.gamesCategories.selectedCid = cid
//   },
//   setUsers(state, { users, type, totalUsers, offset = 0 }) {
//     const validTypes = [
//       'user-friends',
//       'user-mutual-friends',
//       'user-subscriptions',
//       'user-followers',
//       'my-friends',
//       'my-subscriptions',
//       'my-followers',
//       'my-blacklist',
//       'search-users',
//     ]
//     const stateName = camelCase(type)
//
//     if (!validTypes.includes(type) && !state[stateName]) {
//       return
//     }
//
//     state[stateName] = { users, offset, totalUsers }
//   },
//   setSearchUsers(state, { users, offset = 0 }) {
//     state.searchUsers = { users, offset }
//   },
//   setMyNews(state, { news, offset }) {
//     state.myNews = { news, offset }
//   },
//   setChats(state, { offset, chats }) {
//     state.chats = { offset, chats }
//   },
//   setSearchChats(state, { chats, offset = 0 }) {
//     state.searchChats = { chats, offset }
//   },
//   setChatMessages(state, { offset, messages }) {
//     state.chatMessages = { offset, messages }
//   },
//   deleteChatMessages(state) {
//     state.chatMessages = { messages: [], offset: 0 }
//   },
//   setChatInfo(state, { user }) {
//     state.chatInfo = { user }
//   },
//   setGroupChatInfo(state, { groupChatInfo }) {
//     state.groupChatInfo = groupChatInfo
//   },
//   setGroupChat(state, { groupChat }) {
//     state.groupChat = groupChat
//   },
//   resetChat(state) {
//     state.chatMessages = { messages: [], offset: 0 }
//     state.chatInfo = null
//   },
//   resetGroupChat(state) {
//     state.groupChatMessages = { messages: [], offset: 0 }
//     state.groupChatInfo = null
//   },
//   addMessage(state, { message }) {
//     state.chatMessages.messages.push(message)
//   },
//   deleteMessage(state, { mid }) {
//     state.chatMessages.messages = state.chatMessages.messages.filter(
//       (message) => message.mid !== mid
//     )
//   },
//   setNotifications(state, { notifications, offset }) {
//     state.notifications = { notifications, offset }
//   },
//   deleteNotification(state, { nid }) {
//     state.notifications.notifications = state.notifications.notifications.filter(
//       (notification) => notification.nid !== nid
//     )
//   },
//   clearNotifications(state) {
//     state.notifications.notifications = []
//   },
//   setGamesCategories(state, { categories }) {
//     state.gamesCategories = { selectedCid: null, categories }
//   },
//   setBalance(state, { balance }) {
//     state.balance = balance
//   },
//   setUserInfo(state, { userInfo }) {
//     state.userInfo = userInfo
//   },
//   setPersonal(state, { personalSettings }) {
//     state.personalSettings = personalSettings
//   },
//   updatePersonal(state, { data }) {
//     state.personalSettings = {
//       ...state.personalSettings,
//       ...data,
//     }
//   },
//   setSearchUsersQuery(state, { query }) {
//     state.searchUsersQuery = query
//   },
//   setSearchChatsQuery(state, { query }) {
//     state.searchChatsQuery = query
//   },
//   updateNotificationsSettings(state, { data }) {
//     state.notificationsSettings = {
//       ...state.notificationsSettings,
//       ...data,
//     }
//   },
//   updateAccess(state, { data }) {
//     state.accessSettings = {
//       ...state.accessSettings,
//       ...data,
//     }
//   },
//   setAccess(state, { accessSettings }) {
//     state.accessSettings = accessSettings
//   },
//   setNotificationsSettings(state, { notificationsSettings }) {
//     state.notificationsSettings = notificationsSettings
//   },
//   setSubjects(state, { subjects }) {
//     state.subjects = subjects
//   },
//   setCountries(state, { countries }) {
//     state.countries = [...countries]
//   },
//   setCities(state, { cities }) {
//     state.cities = [...cities]
//   },
//   hideMenu(state, hide) {
//     state.layout.hideMenu = hide
//   },
//   showModalPage(state, show) {
//     state.showModalPage = show
//   },
//   setArticle(state, { name, content }) {
//     state.articles[name] = content
//   },
// }
//
// export const getters = {
//   getMyFriendByUid: (state) => ({ uid }) => {
//     return state.myFriends.users.find((user) => user.uid === uid)
//   },
//   getMyFollowerByUid: (state) => ({ uid }) => {
//     return state.myFollowers.users.find((user) => user.uid === uid)
//   },
//   getMyBlacklistByUid: (state) => ({ uid }) => {
//     return state.myBlacklist.users.find((user) => user.uid === uid)
//   },
//   getChatByUid: (state) => ({ uid }) => {
//     return state.chats.chats.find((chat) => chat.message.user.uid === uid)
//   },
//   getMutualFriends: (state) => ({ users }) => {
//     const uids = map(users, 'uid')
//
//     return state.myFriends.users.find((user) => uids.includes(user.uid))
//   },
//   filteredMyGamesByCid: (state) => ({ cid }) => {
//     return (
//       state.games.myGames.games &&
//       state.games.myGames.games.filter((game) => game.type.cid === cid)
//     )
//   },
//   filteredGamesByCid: (state) => ({ cid }) => {
//     return (
//       state.games.all.games &&
//       state.games.all.games.filter((game) => game.type.cid === cid)
//     )
//   },
//   noEmptyGamesCategories: (state) => {
//     return (
//       state.gamesCategories.categories &&
//       state.gamesCategories.categories.filter(
//         (category) => category?.totalGames !== 0
//       )
//     )
//   },
//   groupedChatMessagesByDate: (state) => {
//     return chain(state.chatMessages.messages)
//       .groupBy('date')
//       .map((value, key) => ({ uuid: nanoid(), date: key, messages: value }))
//       .value()
//   },
//   getChatUserByUid: (state) => ({ uid }) => {
//     return state.chats.chats.find((chat) => chat.message.user.uid === uid)
//   },
//   authUser: (state) => {
//     return { ...camelCaseKeys(state.auth.user, { deep: true }) }
//   },
//   loggedIn: (state) => {
//     if (!state.auth) {
//       return false
//     }
//
//     return state.auth.loggedIn
//   },
//   themeValue: (state) => {
//     if (!state.auth.user) {
//       return '0'
//     }
//
//     return state.auth.user.theme
//   },
//   theme: (state) => {
//     if (!state.auth.user) {
//       return 'day'
//     }
//
//     return state.auth.user.theme === '0' ? 'day' : 'night'
//   },
// }
//
// export const actions = {
//   async toggleTheme({ commit, state, dispatch, getters }, { value }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, theme: value }),
//       })
//
//       const themeResponse = await this.$axios.$post(
//         '/api/settings.theme.switch',
//         formData
//       )
//
//       const { success } = themeResponse?.response
//       const { error } = themeResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('setTheme', { value })
//     } catch (error) {
//       dispatch('toastError', { error })
//     }
//   },
//   async restorePassword({ dispatch }, { emailOrPhone }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, emailorphone: emailOrPhone }),
//       })
//
//       const restoreResponse = await this.$axios.$post(
//         '/api/user.restore',
//         formData
//       )
//
//       const { success } = restoreResponse?.response
//       const { error } = restoreResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//
//       return restoreResponse
//     } catch (error) {
//       dispatch('toastError', { error })
//     }
//   },
//   async getUserGames({ dispatch, commit, state }, { uid, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid, offset }),
//       })
//
//       const gamesResponse = await this.$axios.$post('/api/user.games', formData)
//
//       const { games } = gamesResponse?.response
//       const formattedGames = games.map(function (game) {
//         return {
//           uuid: nanoid(),
//           ...game,
//         }
//       })
//
//       return { offset, games: [...camelCaseKeys(formattedGames)] }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getUserFollowers({ dispatch, commit, state }, { uid, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           uid,
//           offset,
//         }),
//       })
//
//       const followersResponse = await this.$axios.$post(
//         '/api/user.followers',
//         formData
//       )
//
//       const { users } = followersResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       return {
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getUserSubscriptions({ dispatch, commit, state }, { uid, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           uid,
//           offset,
//         }),
//       })
//
//       const subscriptionsResponse = await this.$axios.$post(
//         '/api/user.subscriptions',
//         formData
//       )
//
//       const { users } = subscriptionsResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       return {
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getUserInfo({ dispatch, commit, state }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//
//       const infoResponse = await this.$axios.$post('/api/user.info', formData)
//
//       return {
//         ...camelCaseKeys(infoResponse?.response, { deep: true }),
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async sendReport({ commit, dispatch }, { uid, type, comment }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid, type, comment }),
//       })
//
//       const reportResponse = await this.$axios.$post(
//         '/api/user.report',
//         formData
//       )
//
//       const { success } = reportResponse?.response
//       const { error } = reportResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', { error })
//     }
//   },
//   async saveNotificationsSettings({ dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken, notificationsSettings } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, ...notificationsSettings }),
//       })
//
//       const saveResponse = await this.$axios.$post(
//         '/api/settings.notifications.save',
//         formData
//       )
//
//       const { success } = saveResponse?.response
//       const { error } = saveResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async login({ commit, dispatch, state }, { emailOrPhone, password }) {
//     try {
//       const loginResponse = await dispatch('loginUser', {
//         emailOrPhone,
//         password,
//       })
//
//       // eslint-disable-next-line camelcase
//       const { success, api_token } = loginResponse?.data?.response
//       const { error } = loginResponse?.data
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       // const userResponse = await dispatch('getUser', { apiToken: api_token })
//       // const { theme } = userResponse?.data?.response
//       //
//       // await dispatch('setUser', {
//       //   user: {
//       //     ...camelCaseKeys(userResponse?.data?.response, { deep: true }),
//       //   },
//       // })
//       commit('setApiToken', { token: api_token })
//       dispatch('toastSuccess', {})
//
//       return loginResponse
//     } catch (error) {
//       dispatch('toastError', {
//         message: 'There was an issue signing in',
//         error,
//       })
//     }
//   },
//   async loginUser({ dispatch }, { emailOrPhone, password }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const formData = getFormData({
//         ...snakeCaseKeys({ emailorphone: emailOrPhone, pass: password }),
//       })
//       const loginResponse = await this.$auth.loginWith('local', {
//         data: formData,
//       })
//
//       const { success } = loginResponse?.data?.response
//       const { error } = loginResponse?.data
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//
//       return loginResponse
//     } catch (error) {
//       dispatch('toastError', { error })
//     }
//   },
//   async sendSupport(
//     { commit, dispatch, state },
//     { subject, email, name, message }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({ apiToken, subject, email, name, message })
//
//       const supportResponse = await this.$axios.$post(
//         '/api/support.send',
//         formData
//       )
//
//       const { success } = supportResponse?.response
//       const { error } = supportResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async setUser({ dispatch, commit }, { user }) {
//     try {
//       await this.$auth.setUser({
//         ...user,
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getUser({ dispatch }, { apiToken }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       return await this.$axios.post('/api/auth', formData)
//     } catch (error) {
//       dispatch('toastError', { error })
//     }
//   },
//   async register(
//     { dispatch },
//     { name, emailOrPhone, password, passwordCheck }
//   ) {
//     try {
//       const registerResponse = await dispatch('registerUser', {
//         name,
//         emailOrPhone,
//         password,
//         passwordCheck,
//       })
//
//       await dispatch('loginUser', { emailOrPhone, password })
//
//       return registerResponse
//     } catch (error) {}
//   },
//   async registerUser(
//     { dispatch },
//     { name, emailOrPhone, password, passwordCheck }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           name,
//           emailorphone: emailOrPhone,
//           pass: password,
//           passCheck: passwordCheck,
//         }),
//       })
//
//       const registerResponse = await this.$axios.$post('/api/signup', formData)
//
//       const { success } = registerResponse?.response
//       const { error } = registerResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', { message: 'Thanks for signing up' })
//
//       return registerResponse
//     } catch (error) {
//       dispatch('toastError', {
//         message: 'There was an issue signing up',
//         error,
//       })
//     }
//   },
//   async logoutUser({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       await this.$auth.logout({
//         data: formData,
//       })
//
//       commit('resetApiToken')
//     } catch (error) {
//       dispatch('toastError', {
//         message: 'There was an issue logging out',
//         error,
//       })
//     }
//   },
//   async addGame({ dispatch, commit, state }, { game }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, gid: game.gid }),
//       })
//
//       const addResponse = await this.$axios.$post('/api/games.add', formData)
//
//       const { success } = addResponse?.response
//       const { error } = addResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('addGame', {
//         game: {
//           uuid: nanoid(),
//           ...game,
//         },
//       })
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async removeGame({ dispatch, commit, state }, { gid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, gid }),
//       })
//
//       const removeResponse = await this.$axios.$post(
//         '/api/games.remove',
//         formData
//       )
//
//       const { success } = removeResponse?.response
//       const { error } = removeResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('removeGame', {
//         gid,
//       })
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async toggleGameNotifications(
//     { dispatch, commit, state },
//     { gid, notifications }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, gid, notifications }),
//       })
//
//       const notificationsResponse = await this.$axios.$post(
//         '/api/games.notifications',
//         formData
//       )
//
//       const { success } = notificationsResponse?.response
//       const { error } = notificationsResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async toggleGameFriends({ dispatch, commit, state }, { gid, friends }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, gid, friends }),
//       })
//
//       const notificationsResponse = await this.$axios.$post(
//         '/api/games.friends',
//         formData
//       )
//
//       const { success } = notificationsResponse?.response
//       const { error } = notificationsResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async runGame({ dispatch, commit, state }, { gid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, gid }),
//       })
//
//       const runResponse = await this.$axios.$post('/api/games.run', formData)
//
//       // eslint-disable-next-line camelcase
//       const { game_url } = runResponse?.response
//       const { error } = runResponse
//
//       // eslint-disable-next-line camelcase
//       if (!game_url) {
//         throw new Error(error)
//       }
//
//       commit('setGame', {
//         game: {
//           ...camelCaseKeys(runResponse?.response),
//           gid,
//         },
//       })
//       dispatch('toastSuccess', {})
//       await this.$router.push({ name: 'game', params: { gid } })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getGames(
//     { getters, dispatch, commit, state },
//     { type = 'all', cid, offset = 0 }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const validTypes = ['recommended', 'new', 'recent', 'carousel', 'all']
//
//       if (!validTypes.includes(type)) {
//         return
//       }
//
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, type: cid, offset }),
//       })
//       const gamesUrl = type === 'all' ? '/api/games' : `/api/games.${type}`
//
//       const gamesResponse = await this.$axios.$post(gamesUrl, formData)
//
//       const { games } = gamesResponse?.response
//       const formattedGames = games.map(function (game) {
//         return {
//           uuid: nanoid(),
//           ...game,
//         }
//       })
//
//       commit('setGames', {
//         type,
//         offset,
//         games: [...camelCaseKeys(formattedGames)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getGamesCategories({ dispatch, commit, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       const categoriesResponse = await this.$axios.$post(
//         '/api/games.categories',
//         formData
//       )
//
//       const { categories } = categoriesResponse?.response
//       const formattedCategories = categories.map(function (category) {
//         return {
//           uuid: nanoid(),
//           ...category,
//         }
//       })
//
//       commit('setGamesCategories', {
//         categories: [...camelCaseKeys(formattedCategories)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getMyNews({ dispatch, commit, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, offset }),
//       })
//
//       const newsResponse = await this.$axios.$post('/api/news.my', formData)
//
//       const { news } = newsResponse?.response
//       const formattedNews = news.map(function (item) {
//         return {
//           uuid: nanoid(),
//           ...item,
//         }
//       })
//
//       commit('setMyNews', {
//         news: [...camelCaseKeys(formattedNews, { deep: true })],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getPersonal({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       const settingsResponse = await this.$axios.$post(
//         '/api/settings.main',
//         formData
//       )
//
//       const { settings } = settingsResponse?.response
//       const formattedPersonalSettings = {
//         ...settings,
//         birth_day: new Date(settings.birth_date).getDate(),
//         birth_month: new Date(settings.birth_date).getMonth(),
//         birth_year: new Date(settings.birth_date).getFullYear(),
//       }
//
//       commit('setPersonal', {
//         personalSettings: {
//           ...camelCaseKeys(formattedPersonalSettings),
//         },
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async savePersonal({ dispatch, commit, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken, personalSettings } = state
//       const date = new Date(
//         Date.UTC(
//           personalSettings.birthYear,
//           personalSettings.birthMonth,
//           personalSettings.birthDay,
//           0,
//           0,
//           0
//         )
//       )
//       const birthDate = date.toLocaleDateString('en-US')
//       const formattedPersonalSettings = {
//         ...personalSettings,
//         birthDate,
//       }
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, ...formattedPersonalSettings }),
//       })
//
//       const saveResponse = await this.$axios.$post(
//         '/api/settings.main.save',
//         formData
//       )
//
//       const { success } = saveResponse?.response.result
//       const { error } = saveResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('updatePersonal', {
//         data: {
//           birthDate,
//         },
//       })
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async saveAccess(
//     { commit, dispatch, state },
//     { email, newEmail, newEmailCheck, password, newPassword, newPasswordCheck }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           email,
//           newEmail,
//           newEmailCheck,
//           pass: password,
//           new_pass: newPassword,
//           new_pass_check: newPasswordCheck,
//         }),
//       })
//
//       const saveResponse = await this.$axios.$post(
//         '/api/settings.access.save',
//         formData
//       )
//
//       const { success } = saveResponse?.response?.result
//       const { error } = saveResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getAccess({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//         }),
//       })
//
//       const settingsResponse = await this.$axios.$post(
//         '/api/settings.access',
//         formData
//       )
//       const { settings } = settingsResponse?.response
//
//       commit('setAccess', {
//         accessSettings: {
//           ...camelCaseKeys(settings),
//         },
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getSubjects({ commit, dispatch }) {
//     try {
//       const subjectsResponse = await this.$axios.$post('/api/support.subjects')
//
//       const { subjects } = subjectsResponse?.response
//
//       commit('setSubjects', {
//         subjects: [...camelCaseKeys(subjects)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getCountries({ commit, dispatch }) {
//     try {
//       const countriesResponse = await this.$axios.$post('/api/util.countries')
//
//       const { countries } = countriesResponse?.response
//
//       commit('setCountries', {
//         countries: [...camelCaseKeys(countries)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getCities({ commit, dispatch }, { countryId }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           countryId,
//         }),
//       })
//
//       const citiesResponse = await this.$axios.$post(
//         '/api/util.cities',
//         formData
//       )
//
//       const { cities } = citiesResponse?.response
//
//       commit('setCities', {
//         cities: [...camelCaseKeys(cities)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getNotificationsSettings({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//         }),
//       })
//
//       const settingsResponse = await this.$axios.$post(
//         '/api/settings.notifications',
//         formData
//       )
//
//       const { settings } = settingsResponse?.response
//
//       commit('setNotificationsSettings', {
//         notificationsSettings: {
//           ...camelCaseKeys(settings),
//         },
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getMyBlacklist({ commit, dispatch, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//         }),
//       })
//
//       const blacklistResponse = await this.$axios.$post(
//         '/api/blacklist.get',
//         formData
//       )
//
//       const { users } = blacklistResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       commit('setUsers', {
//         type: 'my-blacklist',
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async addMyBlacklist({ dispatch, commit, getters, state }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const user = getters.getMyFriendByUid({ uid })
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//
//       const addResponse = await this.$axios.$post(
//         '/api/blacklist.add',
//         formData
//       )
//
//       const { success } = addResponse?.response
//       const { error } = addResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('addUser', {
//         type: 'my-blacklist',
//         user: {
//           ...user,
//           uuid: nanoid(),
//         },
//       })
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async removeMyBlacklist({ dispatch, commit, state }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//
//       const removeResponse = await this.$axios.$post(
//         '/api/blacklist.remove',
//         formData
//       )
//
//       const { success } = removeResponse?.response
//       const { error } = removeResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('removeUser', {
//         type: 'my-blacklist',
//         uid,
//       })
//
//       dispatch('toastSuccess', {})
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getFriends({ commit, dispatch, state }, { uid, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           uid,
//           offset,
//         }),
//       })
//
//       const friendResponse = await this.$axios.$post(
//         '/api/friends.get',
//         formData
//       )
//
//       const { users } = friendResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       return {
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       }
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getMutualFriends({ commit, dispatch, state }, { uid, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           uid,
//           offset,
//         }),
//       })
//
//       const mutualResponse = await this.$axios.$post(
//         '/api/friends.mutual',
//         formData
//       )
//
//       const { users } = mutualResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       return {
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       }
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getMyFollowers({ commit, dispatch, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           type: 0,
//           offset,
//         }),
//       })
//
//       const requestsResponse = await this.$axios.$post(
//         '/api/friends.requests',
//         formData
//       )
//
//       const { users } = requestsResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       commit('setUsers', {
//         type: 'my-followers',
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getMySubscriptions({ commit, dispatch, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           type: 1,
//           offset,
//         }),
//       })
//
//       const requestsResponse = await this.$axios.$post(
//         '/api/friends.requests',
//         formData
//       )
//
//       const { users } = requestsResponse?.response
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       commit('setUsers', {
//         type: 'my-subscriptions',
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async addFriend({ commit, dispatch, state, getters }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//
//       const addResponse = await this.$axios.$post('/api/friends.add', formData)
//
//       const { success } = addResponse?.response
//       const { error } = addResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       const user = getters.getMyFollowerByUid({ uid })
//
//       commit('removeUser', { type: 'my-followers', uid })
//       commit('addUser', { type: 'my-friends', user })
//
//       return addResponse
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async deleteFriend({ commit, dispatch, state }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/friends.del',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       const user = getters.getMyFriendByUid({ uid })
//
//       commit('removeUser', { type: 'my-friends', uid })
//       commit('addUser', { type: 'my-followers', user })
//
//       return deleteResponse
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getChats({ commit, dispatch, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           offset,
//         }),
//       })
//
//       const chatsResponse = await this.$axios.$post('/api/chats', formData)
//
//       const { chats } = chatsResponse?.response
//
//       commit('setChats', {
//         offset,
//         chats: [...camelCaseKeys(chats, { deep: true })],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getGroupChatInfo({ dispatch, commit, state }, { cid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid }, { deep: true }),
//       })
//
//       const infoResponse = await this.$axios.$post('/api/chat.info', formData)
//
//       const { chat } = infoResponse?.response
//
//       commit('setGroupChatInfo', {
//         groupChatInfo: {
//           ...camelCaseKeys(chat),
//         },
//       })
//       await this.$router.push({
//         path: 'group-chat-info',
//         params: { cid: chat.cid },
//       })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async createChat({ dispatch, commit, state, getters }, { uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, uid }),
//       })
//       const userChat = getters.getChatByUid({ uid })
//
//       if (userChat.length) {
//         await this.$router.push({
//           name: 'chat-cid',
//           params: { cid: userChat.cid },
//         })
//
//         return
//       }
//
//       const createResponse = await this.$axios.$post(
//         '/api/chat.create',
//         formData
//       )
//
//       const { chat, success } = createResponse?.response
//       const { error } = createResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('setGroupChat', {
//         groupChat: {
//           ...camelCaseKeys(chat),
//         },
//       })
//       dispatch('toastSuccess', {})
//       await this.$router.push({ name: 'chat-cid', params: { cid: chat.cid } })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async createGroupChat(
//     { dispatch, commit, state },
//     { title = '', uids = [] }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, title, uids }),
//       })
//
//       const createResponse = await this.$axios.$post(
//         '/api/chat.create',
//         formData
//       )
//
//       const { chat, success } = createResponse?.response
//       const { error } = createResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('setGroupChat', {
//         groupChat: {
//           ...camelCaseKeys(chat),
//         },
//       })
//       dispatch('toastSuccess', {})
//       await this.$router.push({ name: 'group-chat', params: { cid: chat.cid } })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async leaveGroupChat({ commit, dispatch, state }, { cid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid }),
//       })
//
//       const leaveResponse = await this.$axios.$post('/api/chat.leave', formData)
//
//       const { success } = leaveResponse?.response
//       const { error } = leaveResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('resetGroupChat')
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async inviteGroupChat({ commit, dispatch, state }, { cid, uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid, uid }),
//       })
//
//       const inviteResponse = await this.$axios.$post(
//         '/api/chat.invite',
//         formData
//       )
//
//       const { success } = inviteResponse?.response
//       const { error } = inviteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async kickGroupChat({ commit, dispatch, state }, { cid, uid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid, uid }),
//       })
//
//       const kickResponse = await this.$axios.$post('/api/chat.kick', formData)
//
//       const { success } = kickResponse?.response
//       const { error } = kickResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async deleteChatMessages({ commit, dispatch, state }, { cid }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/messages.delete',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('deleteChatMessages')
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async sendMessage({ commit, dispatch, state }, { cid, uid, text }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, cid, uid, text }),
//       })
//
//       const sendResponse = await this.$axios.$post(
//         '/api/message.send',
//         formData
//       )
//
//       const { success, message } = sendResponse?.response
//       const { error } = sendResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('addMessage', {
//         message: {
//           uuid: nanoid(),
//           ...message,
//         },
//       })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async deleteMessage({ commit, dispatch, state }, { mid, deleteCode }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, mid, delete: deleteCode }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/message.delete',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('deleteMessage', {
//         mid,
//       })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getArticle({ commit, dispatch, state }, { name = 'about' }) {
//     try {
//       const articleResponse = await this.$axios.$post(`/api/article.${name}`)
//
//       const { content } = articleResponse?.response
//
//       commit('setArticle', {
//         name,
//         content: {
//           ...camelCaseKeys(content),
//         },
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getChatMessages({ commit, dispatch, state }, { cid, uid, offset }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           cid,
//           uid,
//           offset,
//         }),
//       })
//
//       const messagesResponse = await this.$axios.$post(
//         '/api/messages',
//         formData
//       )
//
//       const { messages } = messagesResponse?.response
//       const formattedMessages = messages.map(function (message) {
//         return {
//           uuid: nanoid(),
//           date: new Date(message.timestamp).toLocaleDateString(),
//           ...message,
//         }
//       })
//
//       commit('setChatMessages', {
//         offset,
//         messages: [...camelCaseKeys(formattedMessages, { deep: true })],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async getNotifications({ dispatch, commit, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           offset,
//         }),
//       })
//
//       const notificationsResponse = await this.$axios.$post(
//         '/api/notifications',
//         formData
//       )
//
//       const { notifications } = notificationsResponse?.response
//       const formattedNotifications = notifications.map(function (notification) {
//         return {
//           uuid: nanoid(),
//           ...notification,
//         }
//       })
//
//       commit('setNotifications', {
//         offset,
//         notifications: [...camelCaseKeys(formattedNotifications)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async deleteNotification({ commit, dispatch, state }, { nid }) {
//     if (!process.browser) {
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, nid }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/notifications.delete',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('deleteNotification', {
//         nid,
//       })
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async clearNotifications({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/notifications.clear',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       commit('clearNotifications')
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async getBalance({ commit, dispatch, state }, { offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           offset,
//         }),
//       })
//
//       const balanceResponse = await this.$axios.$post(
//         '/api/user.balance',
//         formData
//       )
//
//       commit('setBalance', {
//         balance: {
//           ...camelCaseKeys(balanceResponse?.response),
//         },
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async searchChats({ commit, dispatch, state }, { query, offset = 0 }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           query,
//           offset,
//         }),
//       })
//
//       const searchResponse = await this.$axios.$post(
//         '/api/chat.search',
//         formData
//       )
//
//       const { chats } = searchResponse?.response
//       const { error } = searchResponse
//
//       if (!chats) {
//         throw new Error(error)
//       }
//
//       const formattedChats = chats.map(function (chat) {
//         return {
//           uuid: nanoid(),
//           ...chat,
//         }
//       })
//
//       commit('setSearchChats', {
//         offset,
//         chats: [...camelCaseKeys(formattedChats)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async searchUsers(
//     { commit, dispatch, state },
//     { query, type = 'all', offset = 0 }
//   ) {
//     if (!process.browser) {
//       return
//     }
//
//     const validTypes = ['all', 'friends', 'subscribers']
//
//     if (!validTypes.includes(type)) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({
//           apiToken,
//           query,
//           type,
//           offset,
//         }),
//       })
//
//       const searchResponse = await this.$axios.$post(
//         '/api/user.search',
//         formData
//       )
//
//       const { users } = searchResponse?.response
//       const { error } = searchResponse
//
//       if (!users) {
//         throw new Error(error)
//       }
//
//       const formattedUsers = users.map(function (user) {
//         return {
//           uuid: nanoid(),
//           ...user,
//         }
//       })
//
//       commit('setSearchUsers', {
//         offset,
//         users: [...camelCaseKeys(formattedUsers)],
//       })
//     } catch (error) {
//       dispatch('logError', { error })
//     }
//   },
//   async uploadAvatar({ commit, dispatch, state }, { file }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, file }),
//       })
//
//       const uploadResponse = await this.$axios.$post(
//         '/api/settings.avatar.upload',
//         formData
//       )
//
//       const { success } = uploadResponse?.response
//       const { error } = uploadResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//
//       return {
//         ...camelCaseKeys(uploadResponse?.response),
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async cropAvatar({ commit, dispatch, state }, { fileName, crop }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken, fileName, crop }),
//       })
//
//       const cropResponse = await this.$axios.$post(
//         '/api/settings.avatar.crop',
//         formData
//       )
//
//       const { success } = cropResponse?.response
//       const { error } = cropResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//
//       return {
//         ...camelCaseKeys(cropResponse?.response),
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   async deleteAvatar({ commit, dispatch, state }) {
//     if (!process.browser) {
//       return
//     }
//
//     try {
//       const { apiToken } = state
//       const formData = getFormData({
//         ...snakeCaseKeys({ apiToken }),
//       })
//
//       const deleteResponse = await this.$axios.$post(
//         '/api/settings.avatar.delete',
//         formData
//       )
//
//       const { success } = deleteResponse?.response
//       const { error } = deleteResponse
//
//       if (!success) {
//         throw new Error(error)
//       }
//
//       dispatch('toastSuccess', {})
//
//       return {
//         ...camelCaseKeys(deleteResponse?.response),
//       }
//     } catch (error) {
//       dispatch('toastError', {
//         error,
//       })
//     }
//   },
//   toastSuccess(context, { message = 'Success' }) {
//     try {
//       message && this.$toast.success(message).goAway(1500)
//     } catch (error) {}
//   },
//   toastError(context, { message = 'Error', error }) {
//     try {
//       message && this.$toast.error(message).goAway(1500)
//       console.log(error.toString())
//     } catch (error) {}
//   },
//   logError(context, { error }) {
//     console.log(error.toString())
//   },
// }
